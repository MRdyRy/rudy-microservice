package com.rudy.microservices.config.twitter.to.kafka.service.init;

public interface StreamInitializer {
    void init();
}
