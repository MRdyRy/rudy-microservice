package com.rudy.microservices.config.twitter.to.kafka.service.runner.impl;

import com.rudy.microservices.config.TwitterToKafkaServiceConfigData;
import com.rudy.microservices.config.twitter.to.kafka.service.listener.TwitterKafkaStatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import twitter4j.FilterQuery;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

import javax.annotation.PreDestroy;
import java.util.Arrays;

@Component
@ConditionalOnProperty(name = "twitter-to-kafka-service.enable-mock-tweets",havingValue = "false",matchIfMissing = true)
public class TwitterStreamKafkaRunner implements StreamRunner{

    private final Logger LOG = LoggerFactory.getLogger(TwitterStreamKafkaRunner.class);

    private final TwitterToKafkaServiceConfigData twitterToKafkaServiceConfigData;

    private final TwitterKafkaStatusListener twitterKafkaStatusListener;

    private TwitterStream twitterStream;

    public TwitterStreamKafkaRunner(TwitterToKafkaServiceConfigData configData, TwitterKafkaStatusListener statusListener){
        this.twitterKafkaStatusListener = statusListener;
        this.twitterToKafkaServiceConfigData = configData;
    }

    @Override
    public void start() throws TwitterException {
        twitterStream =  new TwitterStreamFactory().getInstance();
        twitterStream.addListener(twitterKafkaStatusListener);
        addFilter();

    }

    @PreDestroy
    public void shutdown(){
        if(twitterStream!=null){
            LOG.info("CLOSING TWIITER STREAM!!");
            twitterStream.shutdown();
        }
    }

    private void addFilter() {
        String[] keywords = twitterToKafkaServiceConfigData.getTwitterKeywords().toArray(new String[0]);
        FilterQuery filterQuery = new FilterQuery(keywords);
        twitterStream.filter(filterQuery);
        LOG.info("Started filtering twiiter from stream for keywords : ", Arrays.toString(keywords));
    }
}
