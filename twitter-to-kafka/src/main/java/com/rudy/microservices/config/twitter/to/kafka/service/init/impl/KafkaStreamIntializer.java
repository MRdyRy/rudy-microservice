package com.rudy.microservices.config.twitter.to.kafka.service.init.impl;

import com.rudy.microservices.config.KafkaConfigData;
import com.rudy.microservices.config.twitter.to.kafka.service.init.StreamInitializer;
import com.rudy.microservices.kafka.admin.client.KafkaAdminClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class KafkaStreamIntializer implements StreamInitializer {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaStreamIntializer.class);

    private final KafkaConfigData kafkaConfigData;

    private final KafkaAdminClient kafkaAdminClient;

    public KafkaStreamIntializer(KafkaConfigData kafkaConfigData, KafkaAdminClient kafkaAdminClient) {
        this.kafkaConfigData = kafkaConfigData;
        this.kafkaAdminClient = kafkaAdminClient;
    }

    @Override
    public void init() {
        kafkaAdminClient.createTopics();
        kafkaAdminClient.checkSchemaRegistry();
        LOG.info("Topics with name {} is ready for operations",kafkaConfigData.getTopicNamesToCreate().toArray());
    }


}
