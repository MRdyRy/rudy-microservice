package com.rudy.microservices.config.twitter.to.kafka.service.runner.impl;

import com.rudy.microservices.config.TwitterToKafkaServiceConfigData;
import com.rudy.microservices.config.twitter.to.kafka.service.exception.TwitterToKafkaServiceException;
import com.rudy.microservices.config.twitter.to.kafka.service.listener.TwitterKafkaStatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

@Component
@ConditionalOnProperty(name = "twitter-to-kafka-service.enable-mock-tweets",havingValue = "true")
public class MockKafkaStreamRunner implements StreamRunner{

    private static final Logger LOG = LoggerFactory.getLogger(MockKafkaStreamRunner.class);

    private final TwitterToKafkaServiceConfigData twitterToKafkaServiceConfigData;
    private final TwitterKafkaStatusListener twitterKafkaStatusListener;
    private static final Random RANDOM = new Random();

    private static final String[] WORDS = new String[]{
            "Lorem",
            "ipsum",
            "dolor",
            "sit",
            "amet,",
            "consectetur",
            "adipiscing",
            "elit,",
            "eiusmod",
            "tempor",
            "incididunt",
            "labore",
            "dolore",
            "magna",
            "aliqua.",
            "enim",
            "minim",
            "veniam,",
            "quis",
            "nostrud",
            "exercitation",
            "ullamco",
            "laboris",
            "nisi",
            "aliquip",
            "commodo",
            "consequat"
    };

    private static final String tweetAsRawJson = "{" +
            "\"create_at\":\"{0}\"," +
            "\"id\":\"{1}\"," +
            "\"text\":{2}\"," +
            "\"user\":\"{\"id\":\"{3}\"}" +
            "}";

    private static final String TWITTER_STATUS_DATE_FORMAT = "EEE MMM dd HH:mm:ss zzz yyyy";

    public MockKafkaStreamRunner(TwitterToKafkaServiceConfigData twitterToKafkaServiceConfigData, TwitterKafkaStatusListener twitterKafkaStatusListener) {
        this.twitterToKafkaServiceConfigData = twitterToKafkaServiceConfigData;
        this.twitterKafkaStatusListener = twitterKafkaStatusListener;
    }

    @Override
    public void start() throws TwitterException {
        String[] keywords = twitterToKafkaServiceConfigData.getTwitterKeywords().toArray(new String[0]);
        int minTweetLength = twitterToKafkaServiceConfigData.getMockMinTweetLength();
        int maxTweetLength = twitterToKafkaServiceConfigData.getMockMaxTweetLength();
        long sleepsTime = 100;
        LOG.info("Starting mock filtering twitter streams for keywords {}"+ Arrays.toString(keywords));
        stimulateTweetStreams(keywords, minTweetLength, maxTweetLength, sleepsTime);
        return;
    }

    private void stimulateTweetStreams(String[] keywords, int minTweetLength, int maxTweetLength, long sleepsTime) {
        LOG.info("Start Stimulate tweet streams......!");
        try {
            Executors.newSingleThreadExecutor().submit(() ->{
                LOG.info("Processing single thread excutor .....!");
                while (true){
                    String formattedTweetAsRawJson = getFormattedTweet(keywords, minTweetLength,maxTweetLength);
                    Status status = TwitterObjectFactory.createStatus(formattedTweetAsRawJson);
                    LOG.info("status ::=======D "+status.getText().toString());
                    twitterKafkaStatusListener.onStatus(status);
                    sleep(sleepsTime);
                }
            });
        } catch (TwitterToKafkaServiceException e) {
            LOG.error("Error creating twitter status : ",e);
            e.printStackTrace();
        }
    }

    private void sleep(long sleepsTime) {
        try{
            Thread.sleep(sleepsTime);
        }catch (InterruptedException e){
            throw new TwitterToKafkaServiceException("Error while sleeping for waiting new status to create !!");
        }
    }

    private String getFormattedTweet(String[] keywords, int minTweetLength, int maxTweetLength) {
        LOG.info("Processing format tweet .....!");
        String[] params = new String[]{
                ZonedDateTime.now().format(DateTimeFormatter.ofPattern(TWITTER_STATUS_DATE_FORMAT, Locale.ENGLISH)),
                String.valueOf(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE)),
                getRandomTweetContent(keywords,minTweetLength,maxTweetLength),
                String.valueOf(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE))
        };

        return formatTweetAsJsonWithParams(params);
    }

    private String formatTweetAsJsonWithParams(String[] params) {
        String tweet = tweetAsRawJson;
        for (int i =0; i < params.length; i++){
            tweet = tweet.replace("{"+i+"}",params[i]);
        }
        LOG.info("format to json....!"+tweet);
        return tweet;
    }

    private String getRandomTweetContent(String[] keywords, int minTweetLength, int maxTweetLength) {
        StringBuilder sbTweet = new StringBuilder();
        int tweetLength = RANDOM.nextInt(maxTweetLength-minTweetLength+1)+minTweetLength;
        return getRandoTweetsContent(keywords, sbTweet, tweetLength);
    }

    private String getRandoTweetsContent(String[] keywords, StringBuilder sbTweet, int tweetLength) {
        for (int i = 0; i< tweetLength; i ++){
            sbTweet.append(WORDS[RANDOM.nextInt(WORDS.length)]).append(" ");
            if(i == tweetLength / 2) {
                sbTweet.append(keywords[RANDOM.nextInt(keywords.length)]).append(" ");
            }
        }
        return sbTweet.toString().trim();
    }
}
