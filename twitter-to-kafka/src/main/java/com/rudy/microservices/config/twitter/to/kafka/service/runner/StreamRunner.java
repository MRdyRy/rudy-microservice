package com.rudy.microservices.config.twitter.to.kafka.service.runner.impl;

import twitter4j.TwitterException;

public interface StreamRunner {
    void start () throws TwitterException;
}
